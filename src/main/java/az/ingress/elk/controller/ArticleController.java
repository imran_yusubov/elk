package az.ingress.elk.controller;

import az.ingress.elk.model.Article;
import az.ingress.elk.service.ArticleService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @PostMapping(path = "articles")
    public ResponseEntity<Article> createArticle(@RequestBody Article article) {
        return new ResponseEntity<>(articleService.create(article), HttpStatus.CREATED);
    }

    @GetMapping(path = "articles/{id}")
    public ResponseEntity<Article> getArticleById(@PathVariable("id") Long id) {
        System.out.println("id"+id);
        Optional<Article> article = articleService.findById(id);
        if (article.isPresent()) {
            return new ResponseEntity<>(article.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(path = "articles/{article-id}")
    public ResponseEntity<Article> updateArticle(@PathVariable("article-id") Long id, @RequestBody Article article) {
        return new ResponseEntity<>(articleService.update(article, id), HttpStatus.OK);
    }

    @DeleteMapping(path = "articles/{article-id}")
    public ResponseEntity<Article> deleteArticleById(@PathVariable("article-id") Long id) {
        articleService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
