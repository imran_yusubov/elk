package az.ingress.elk.repository;

import az.ingress.elk.model.Article;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ArticleRepository extends PagingAndSortingRepository<Article, Long> {

    List<Article> findTop10ByTitleContainingIgnoreCaseOrContentContainingIgnoreCase(String title,
            String content);

    Optional<Article> findById(Long id);
}
