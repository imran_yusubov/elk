package az.ingress.elk.service;

import az.ingress.elk.model.Article;
import java.util.List;
import java.util.Optional;

/*
 * This interface provides all methods to access the functionality. See ArticleServiceImpl for implementation.
 * 
 * @author crossover
 */
public interface ArticleService {
	/*
	 * Save the default article.
	 */
	 Article create(Article article);

	 Article update(Article article, Long id);
	
	/*
	 * FindById will find the specific user form list.
	 * 
	 */
	Optional<Article> findById(Long id);
	
	/*
	 * Delete a particular article with id
	 */
	void delete(Long id);

}
