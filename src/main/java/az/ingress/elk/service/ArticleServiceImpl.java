package az.ingress.elk.service;

import az.ingress.elk.model.Article;
import az.ingress.elk.repository.ArticleRepository;
import java.time.LocalDateTime;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    @CachePut(value = "article-single", key = "#article.id")
    public Article create(Article article) {
        System.out.println("Adding article");
        article.setModified(LocalDateTime.now());
        return articleRepository.save(article);
    }

    @Override
    @CacheEvict(value = "article-single", key = "#id")
    @Transactional
    public Article update(Article article, Long id) {
        Optional<Article> articleOptional = articleRepository.findById(id);
        if (articleOptional.isPresent()) {
            article.setId(id);
            article.setModified(LocalDateTime.now());
            return articleRepository.save(article);
        }
        return create(article);
    }

    @Override
    @Cacheable(value = "article-single", key = "#id")
    public Optional<Article> findById(Long id) {
        System.out.println("Find article by id :" + id);
        return articleRepository.findById(id);
    }

    @Override
    @CacheEvict(value = "article-single", key = "#id")
    @Transactional
    public void delete(Long id) {
        Optional<Article> articleOptional = articleRepository.findById(id);
        if (articleOptional.isPresent()) {
            articleRepository.delete(articleOptional.get());
        }
    }

}